CKEditor Pastetools
====================

INTRODUCTION
------------

This module integrates the [pastetools](
https://ckeditor.com/cke4/addon/pastetools) CKEditor plugin for Drupal 8.

This plugin allows to create custom paste handling logic for a given paste type. It is used in the Paste from Word and Paste from Google Docs implementations.

It supports filtering content with Advanced Content Filter and custom filter files.

REQUIREMENTS
------------

* CKEditor Module (Core)


INSTALLATION
------------

### Install via Composer (recommended)

If you use Composer to manage dependencies, edit composer.json as follows.

* Run `composer require --prefer-dist composer/installers` to ensure you have
the composer/installers package. This facilitates installation into directories
other than vendor using Composer.

* In composer.json, make sure the "installer-paths" section in "extras" has an
entry for `type:drupal-library`. For example:

```json
{
  "libraries/{$name}": ["type:drupal-library"]
}
```

* Add the following to the "repositories" section of composer.json:

```json
{
  "type": "package",
  "package": {
    "name": "ckeditor/pastetools",
    "version": "4.13.0",
    "type": "drupal-library",
    "extra": {
      "installer-name": "ckeditor/plugins/pastetools"
    },
    "dist": {
      "url": "https://github.com/jdetourris/pastetools/archive/master.zip",
      "type": "zip"
    }
  }
}
```

* Run `composer require 'ckeditor/pastetools:4.13.0'` to download the plugin.

* Run `composer require 'drupal/ckeditor_pastetools:^1.0.0'` to download the
CKEditor Pastetools module, and enable it [as per usual](
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).


### Install Manually

* Download the [pastetools](https://github.com/jdetourris/pastetools/archive/master.zip)
CKEditor plugin.

* Extract and place the plugin contents in the following directory:
`/libraries/ckeditor/plugins/pastetools/`.

* Install the CKEditor Pastetools module [as per usual](
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

MAINTAINERS
-----------
Current maintainers:

 * Julien de Nas de Tourris ([julien](https://www.drupal.org/u/julien))
