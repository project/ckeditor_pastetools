<?php

namespace Drupal\ckeditor_pastetools\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Pastetools" plugin.
 *
 * @CKEditorPlugin(
 *   id = "pastetools",
 *   label = @Translation("Pastetools")
 * )
 */
class CKEditorPastetools extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/ckeditor/plugins/pastetools/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'ckeditor_pastetools') . '/icons';
    return [
      'Pastetools' => [
        'label' => t('Pastetools'),
        'image' => $path . '/pastetools.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
